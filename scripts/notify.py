import os
import sys
import requests


def notify(version: str, url: str, type_network: str):
    payload = {
        # "content": "message content",
        # "username": "custom username",
        "embeds": [
            {
                "title": f"New release v{version} is now available on {type_network}!",
                "description": f"You can easily experiment using docker\n\
            `docker run lunesnode-testnet:{version}`",
                "url": "https://gitlab.com/lunes/blockchain/production/pipeline-blockchain.git",
            }
        ],
    }

    header = {
        "Content-Type": "application/json"
    }
    try:
        requests.post(url, headers=header, json=payload)
    except:
        raise Exception("[ ERROR ] don't notify")


if __name__ == '__main__':
    version, url, type_network = sys.argv[1:]
    print(version, url, type_network)
    notify(version, url, type_network)
