# Lunes Node Pipelines
[![pipeline status](https://gitlab.com/lunes/blockchain/production/pipeline-blockchain/badges/main/pipeline.svg)](https://gitlab.com/lunes/blockchain/production/pipeline-blockchain/-/commits/main)

Esteira de entrega automatizada


### WorkFlow
- Criar uma *issue* descrevendo a nova *feature*, apartir dai criar o *merge-resquest* e *branch*
- A nova *branch* deve se chamar `feature/MINHA-FEATURE`
    - Quando a *feature* estiver pronta faça o `push`

### feature/*
- Quando qualquer *branch* `feature/*` recebe um `push` é iniciado a bateria de  testes unitários
    - Se os testes passarem é criado um `merge-request` para a *branch* `test-ci`

### test-ci
- Caso o *merge-request* seja aprovado é criad um artefato em `TESTNET`
    - É gerado uma imagem `docker` em `TESTNET`
    - Depois é feito o `push` dessa imagem
    - Por fim é criado um `merge-request` para *branch* `main`

### main
- Assim que a *branch* `main` recebe um `merge-request` é criado um novo artefato em `MAINNET`
    - É criado uma nova imagem `docker` agora em `MAINNET`
    - Depois é feito o `push` dessa nova imagem
    - Por fim é enviando notificações para a comunidade